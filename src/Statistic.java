import java.util.*;

/**
 * Contains information about word occurrence
 */
public class Statistic
{
    private Map<String, Integer> words = new HashMap<String, Integer>();
    private int wordCount = 0;

    public Statistic()
    {

    }

    /**
     * Add word into statistic
     *
     * @param word adding word
     */
    public void putWord(String word)
    {
        if (words.containsKey(word)) {
            int valueWords = words.get(word);
            valueWords++;
            words.put(word, valueWords);
        } else {
            words.put(word, 1);
        }
        wordCount++;
    }

    public int getWordCount()
    {
        return this.wordCount;
    }

    /**
     * @return word stat ordered by word occurrence descending
     */
    public List<Map.Entry<String, Integer>> getOrderedStatistic()
    {
        List<Map.Entry<String, Integer>> list =
                new LinkedList<Map.Entry<String, Integer>>(words.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2)
            {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        return list;
    }


}
