import java.io.*;

final public class Main
{
    private static final int INPUT_FILE = 0;
    private static final int OUTPUT_FILE = 1;
    private static final int COUNT_OF_ARGUMENTS = 2;

    public static void main( String[] args )
    {
        if( COUNT_OF_ARGUMENTS != args.length ) {
            System.out.println("Usage: inputFile outputFile");
            return;
        }

        InputStreamReader reader = null;
        OutputStreamWriter writer = null;

        try {
            File inputFile = new File(args[INPUT_FILE]);
            File outputFile = new File(args[OUTPUT_FILE]);

            reader = new InputStreamReader(new FileInputStream(inputFile));
            writer = new OutputStreamWriter(new FileOutputStream(outputFile));
            startWork(reader, writer);
        } catch (IOException e) {
            System.err.println("IO error occurred: " + e.getLocalizedMessage());
        } finally {
            try {
                if (reader != null && writer!= null) {
                    reader.close();
                    writer.close();
                }
            } catch (IOException ignored) {
                // Do nothing
            }
        }
    }

    private static void startWork(InputStreamReader reader, OutputStreamWriter writer) throws IOException {
        Statistic statistic = new Statistic();
        FileReader statisticReader= new FileReader(reader);
        statisticReader.read(statistic);

        FileWriter statisticWriter = new FileWriter(writer);
        statisticWriter.write(statistic);
    }
}