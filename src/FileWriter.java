import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

public final class FileWriter {
    private static final String CSV_DELIMITER = ";";
    private static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private final Writer writer;

    public FileWriter(Writer writer) {
        this.writer = writer;
    }

    public void write(Statistic statistic) throws IOException {
        assert null != statistic;
        int wordCount = statistic.getWordCount();
        List<Map.Entry<String, Integer>> orderStat = statistic.getOrderedStatistic();
        writeStat(wordCount, orderStat);
    }

    private void writeStat(int wordCount, List<Map.Entry<String, Integer>> orderStat) throws IOException {
        DecimalFormat myFormatter = new DecimalFormat("#.##");
        for( Map.Entry<String, Integer> wordStat: orderStat) {
            assert (0 != wordCount);
            double relativeOccurrence = (double) wordStat.getValue() / (double) wordCount;

            StringBuilder builder = new StringBuilder();
            builder.append(wordStat.getKey()).append(CSV_DELIMITER).
                    append(wordStat.getValue()).append(CSV_DELIMITER).
                    append(myFormatter.format(relativeOccurrence)).append(LINE_SEPARATOR);

            writer.write(builder.toString());
        }

        writer.flush();
    }
}
