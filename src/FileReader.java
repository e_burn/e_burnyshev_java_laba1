import java.io.IOException;
import java.io.InputStreamReader;

final public class FileReader
{
    private final InputStreamReader streamReader;

    public FileReader(InputStreamReader streamReader)
    {
        assert null != streamReader;
        this.streamReader = streamReader;
    }

    public void read(Statistic statistic) throws IOException
    {
        assert null!=statistic: "Statistic is null";
        int readSymbol = 0;
        StringBuilder builder = new StringBuilder();

        readSymbol = streamReader.read();
        while ( 0 <= readSymbol) {
            char readChar = (char) readSymbol;
            if (isDelimiter(readChar)) {
                processReadString(statistic, builder);
                builder.setLength(0);
            } else {
                builder.append(readChar);
            }
            readSymbol = streamReader.read();
        }
        processReadString(statistic, builder);
    }

    private static void processReadString(Statistic statistic, StringBuilder builder)
    {
        assert null != statistic;
        assert null != builder;

        String currentString = builder.toString();
        if (!currentString.isEmpty()) {
            statistic.putWord(currentString);
        }
    }

    private static boolean isDelimiter(char symbol)
    {
        return !Character.isLetterOrDigit(symbol);
    }


}
